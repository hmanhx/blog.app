from django.urls import path

from .views import (
    BlogListView,
    BlogListDetail,
    BlogCreateNew,
    BlogUpdateView,
    BlogDeleteView
)

urlpatterns = [
    path('post/<int:pk>/delete/', BlogDeleteView.as_view(), name='post_delete'),
    path('post/<int:pk>/edit/', BlogUpdateView.as_view(), name='post_edit'),
    path('post/new/', BlogCreateNew.as_view(), name='post_new'),
    path('post/<int:pk>/', BlogListDetail.as_view(), name='post_detail'),
    path('', BlogListView.as_view(), name='home'),
]
